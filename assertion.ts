export function getResults<T extends any[], V>(testVals: [T, V][], func: (...val: T) => V) {
  let i = 0;
  for (const params of testVals) {
    i++;
    console.log(`\u001b[1;33mTest #${i}`);

    const args = params[0];
    const comp = params[1];

    console.time("result time");

    const result = func(...args);

    if (assertByVal(result, comp)) {
      console.log(`\u001b[1;36mvalue = ${args}; result = ${result}`);
    } else {
      console.log(`\u001b[1;31mvalue = ${args}; result = ${result} expected: ${comp}`);
    }

    console.timeEnd("result time");
    console.log("\n\r");
  }
}

export function assertByVal<T>(val: T, comp: T): val is T {
  if (Array.isArray(comp) && Array.isArray(val) && val.length == comp.length) {
    return val.every((elem) => comp.find((elem2) => elem2 == elem) !== -1);
  }
  return val == comp;
}
