// https://leetcode.com/problems/longest-substring-without-repeating-characters/submissions/894622192/

import { getResults } from "./assertion";

function lengthOfLongestSubstring(s: string): number {
  if (s.length == 1) return 1;
  let maxLength = 0;
  let str = s[0];
  let i = 0;
  let j = 1;
  while (j < s.length) {
    str = s.substring(i, j);
    const pos = str.indexOf(s[j]);
    if (pos !== -1) {
      i = i + pos + 1;
    }
    maxLength = maxLength < j - i + 1 ? j - i + 1 : maxLength;
    j++;
  }

  return maxLength;
}

type TArgs = [string];
type TRes = number;
type TIn = [TArgs, TRes];

const testVals: TIn[] = [
  [["abcabcbb"], 3],
  [["bbbbb"], 1],
  [["pwwkew"], 3],
  [["dvdf"], 3],
  [[" "], 1],
];

getResults<TArgs, TRes>(testVals, lengthOfLongestSubstring);
