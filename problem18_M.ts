// https://leetcode.com/problems/4sum/submissions/895236083/

import { getResults } from "./assertion";

function fourSum(nums: number[], target: number): number[][] {
  const result: number[][] = [];
  const sorted = nums.sort((a, b) => a - b);
  for (let i = 0; i < sorted.length - 3; i++) {
    if (i > 0 && sorted[i] == sorted[i - 1]) continue;
    for (let j = i + 1; j < sorted.length - 2; j++) {
      let k = j + 1;
      let l = sorted.length - 1;
      while (k < l) {
        const sum = sorted[i] + sorted[j] + sorted[k] + sorted[l];
        if (sum === target) {
          result.push([sorted[i], sorted[j], sorted[k], sorted[l]]);
        }

        if (sum < target) {
          while (sorted[k] === sorted[k + 1]) k++;
          k++;
        } else {
          while (sorted[l] === sorted[l - 1]) l--;
          l--;
        }
      }
    }
  }
  return result
    .map((elem) => JSON.stringify(elem))
    .filter((e, i, a) => i === a.indexOf(e))
    .map((elem) => JSON.parse(elem));
}

type TArgs = [number[], number];
type TRes = number[][];
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [
    [[1, 0, -1, 0, -2, 2], 0],
    [
      [-2, -1, 1, 2],
      [-2, 0, 0, 2],
      [-1, 0, 0, 1],
    ],
  ],
  [[[2, 2, 2, 2, 2], 8], [[2, 2, 2, 2]]],
  [
    [[-2, -1, -1, 1, 1, 2, 2], 0],
    [
      [-2, -1, 1, 2],
      [-1, -1, 1, 1],
    ],
  ],
];

getResults<TArgs, TRes>(vals, fourSum);
