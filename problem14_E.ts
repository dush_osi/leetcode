// https://leetcode.com/problems/longest-common-prefix/
import { getResults } from "./assertion";

function longestCommonPrefix(strs: string[]): string {
  if (strs.length === 0) return "";

  let currPos = 0;

  while (currPos < strs[0].length) {
    for (const st of strs) {
      if (st[currPos] != strs[0][currPos]) {
        return strs[0].slice(0, currPos);
      }
    }
    currPos++;
  }
  return strs[0].slice(0, currPos);
}

type TArgs = [string[]];
type TRes = string;
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [[["flower", "flow", "flight"]], "fl"],
  [[["dog", "racecar", "car"]], ""],
];

getResults<TArgs, TRes>(vals, longestCommonPrefix);
