// https://leetcode.com/problems/integer-to-roman/
import { getResults } from "./assertion";

const romans: Record<number, string> = {
  1: "I",
  2: "II",
  3: "III",
  4: "IV",
  5: "V",
  9: "IX",
  10: "X",
  40: "XL",
  50: "L",
  90: "XC",
  100: "C",
  400: "CD",
  500: "D",
  900: "CM",
  1000: "M",
};

function intToRoman(num: number): string {
  if (num > 3999) return "";
  let result = "";
  let digs = num.toString().split("");
  const len = digs.length - 1;
  for (let i = 0; i <= len; i++) {
    if (romans[+digs[i] * Math.pow(10, len - i)]) {
      result = result + romans[+digs[i] * Math.pow(10, len - i)];
    } else {
      let dig = +digs[i];
      if (+digs[i] >= 5) {
        result += romans[5 * Math.pow(10, len - i)];
        dig = +digs[i] - 5;
      }
      for (let j = 1; j <= dig; j++) {
        result += romans[Math.pow(10, len - i)];
      }
    }
  }
  return result;
}

type TArgs = [number];
type TRes = string;
type TIn = [TArgs, TRes];

const testVals: TIn[] = [
  [[3], "III"],
  [[58], "LVIII"],
  [[74], "LXXIV"],
  [[1994], "MCMXCIV"],
  [[3994], "MMMCMXCIV"],
  [[394], "CCCXCIV"],
  [[794], "DCCXCIV"],
  [[894], "DCCCXCIV"],
  [[7994], ""],
];

getResults<TArgs, TRes>(testVals, intToRoman);
