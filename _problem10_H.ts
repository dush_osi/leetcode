import { getResults } from "./assertion";

function isMatch(s: string, p: string): boolean {
  let result = true;

  const posAster = p.indexOf("*");
  const posDot = p.indexOf(".");
  if (posAster == -1 && posDot == -1) return s == p;
  if (posAster > 0) {
    const char = p[posAster - 1];
    const firstChar = s.indexOf(char);
    for (let i = firstChar + 1; i < s.length; i++) {
      if (s[i] != char) return false;
    }
  }
  return result;
}

type TArgs = [string, string];
type TRes = boolean;

const testVals: [TArgs, TRes][] = [
  [["aa", "a"], false],
  [["aa", "a*"], true],
  [["a", "a*"], true],
  [["ab", ".*"], true],
];

getResults<TArgs, TRes>(testVals, isMatch);
