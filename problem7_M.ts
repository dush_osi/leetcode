// https://leetcode.com/problems/reverse-integer/
import { getResults } from "./assertion";

function reverse(x: number): number {
  let result = 0;
  const negative = x < 0 ? -1 : 1;
  x = Math.abs(x);
  while (x > 0) {
    const newNum = x % 10;
    result = result * 10 + newNum;
    x = (x - newNum) / 10;
  }
  return result < -2147483648 || result > 2147483648 - 1 ? 0 : negative * result;
}

type TArgs = [number];
type TRes = number;
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [[123], 321],
  [[-123], -321],
  [[120], 21],
];
getResults<TArgs, TRes>(vals, reverse);
