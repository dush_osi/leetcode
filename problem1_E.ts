// https://leetcode.com/problems/two-sum/
import { getResults } from "./assertion";

function twoSum(nums: number[], target: number): number[] {
  for (let i = 0; i < nums.length - 1; i++) {
    for (let j = i + 1; j < nums.length; j++) {
      if (nums[i] + nums[j] === target) return [i, j];
    }
  }
  return [];
}

type TArgs = [number[], number];
type TRes = number[];
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [
    [[2, 7, 11, 15], 9],
    [0, 1],
  ],
  [
    [[3, 2, 4], 6],
    [1, 2],
  ],
  [
    [[3, 3], 6],
    [0, 1],
  ],
];
getResults<TArgs, TRes>(vals, twoSum);
