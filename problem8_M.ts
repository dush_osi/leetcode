// https://leetcode.com/problems/string-to-integer-atoi/
import { getResults } from "./assertion";

function isDigit(s: string): boolean {
  return s >= "0" && s <= "9";
}
function isOperation(s: string): boolean {
  return s == "+" || s == "-";
}
function myAtoi(s: string): number {
  const tmp = s.trim();
  let result = 0;

  if (isDigit(tmp[0]) || (isOperation(tmp[0]) && isDigit(tmp[1]))) {
    for (let i = 0; i < tmp.length; i++) {
      if (isDigit(tmp[i])) {
        result = result * 10 + +tmp[i];
        if (i + 1 >= tmp.length || !isDigit(tmp[i + 1])) {
          result *= tmp[0] == "-" ? -1 : 1;
          if (result >= Math.pow(2, 31)) return Math.pow(2, 31) - 1;
          if (result < Math.pow(-2, 31)) return Math.pow(-2, 31);
          return result;
        }
      }
    }
  }
  return 0;
}

type TArgs = [string];
type TRes = number;
type TIn = [TArgs, TRes];

const testVals: TIn[] = [
  [["42"], 42],
  [["   -42"], -42],
  [["4193 with words"], 4193],
  [["words and 987"], 0],
  [["-91283472332"], -2147483648],
  [["-+12"], 0],
  [["+1"], 1],
];

getResults<TArgs, TRes>(testVals, myAtoi);
