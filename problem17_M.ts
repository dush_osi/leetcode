// https://leetcode.com/problems/letter-combinations-of-a-phone-number/
import { getResults } from "./assertion";

const digAlpha: Record<string, string[]> = {
  "2": ["a", "b", "c"],
  "3": ["d", "e", "f"],
  "4": ["g", "h", "i"],
  "5": ["j", "k", "l"],
  "6": ["m", "n", "o"],
  "7": ["p", "q", "r", "s"],
  "8": ["t", "u", "v"],
  "9": ["w", "x", "y", "z"],
};

function letterCombinations(digits: string): string[] {
  let result: string[] = [];
  const digs = digits.split("");
  for (let dig of digs) {
    const tmp = [];
    const alphas = digAlpha[dig];
    if (result.length == 0) {
      result = [...alphas];
    } else {
      for (const alpha of alphas) {
        for (const elem of result) {
          tmp.push(elem + alpha);
        }
      }
      result = [...tmp];
    }
  }
  return result;
}

type TArgs = [string];
type TRes = string[];
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [["23"], ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]],
  [[""], []],
  [["2"], ["a", "b", "c"]],
];
getResults<TArgs, TRes>(vals, letterCombinations);
