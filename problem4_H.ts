import { getResults } from "./assertion";
function findMedianSortedArrays(nums1: number[], nums2: number[]): number {
  const merged = [...nums1, ...nums2].sort((a, b) => a - b);
  if (merged.length === 1) return merged[0];
  const median = merged.length / 2;
  if (merged.length % 2 === 0) {
    return (merged[median - 1] + merged[median]) / 2;
  } else {
    return merged[median - 0.5];
  }
}

type TArgs = [number[], number[]];
type TRes = number;
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [[[1, 3], [2]], 2],
  [
    [
      [1, 2],
      [3, 4],
    ],
    2.5,
  ],
  [[[], [1]], 1],
  [[[], [1, 2, 3, 4, 5]], 3],
];
getResults<TArgs, TRes>(vals, findMedianSortedArrays);
