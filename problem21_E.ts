//https://leetcode.com/problems/merge-two-sorted-lists/submissions/897238291/

import { ListNode } from "./classes";

function mergeTwoLists(list1: ListNode | null, list2: ListNode | null): ListNode | null {
  let result: ListNode | null = new ListNode(-1);
  let node: ListNode | null = result;
  while (list1 !== null && list2 !== null) {
    if (list1.val < list2.val) {
      node.next = list1;
      list1 = list1.next;
    } else {
      node.next = list2;
      list2 = list2.next;
    }
    node = node.next;
  }
  node.next = list1 ? list1 : list2;
  return result.next;
}

let list1: ListNode | null = ListNode.prepareList([1, 2, 4]);
let list2: ListNode | null = ListNode.prepareList([1, 3, 4]);

const result = mergeTwoLists(list1, list2);
console.log(ListNode.prepareAnswer(result));
