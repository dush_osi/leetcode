// https://leetcode.com/problems/3sum/
import { getResults } from "./assertion";

function threeSum(nums: number[]): any {
  let result = [];
  const sorted = nums.sort((a, b) => a - b);

  for (let i = 0; i < sorted.length - 2; i++) {
    if (i > 0 && sorted[i] == sorted[i - 1]) continue;
    let j = i + 1;
    let k = sorted.length - 1;
    while (j < k) {
      const sum = sorted[i] + sorted[j] + sorted[k];
      if (sum === 0) {
        const tmp = [sorted[i], sorted[j], sorted[k]];
        result.push(tmp);
        while (sorted[j] === sorted[j + 1]) j++;
        while (sorted[k] === sorted[k - 1]) k--;
        j++;
        k--;
      } else if (sum < 0) {
        j++;
      } else {
        k--;
      }
    }
  }
  return result;
}

type TArgs = [number[]];
type TRes = number[][];
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [
    [[-1, 0, 1, 2, -1, -4]],
    [
      [-1, 0, 1],
      [-1, -1, 2],
    ],
  ],
  [[[0, 1, 1]], []],
  [[[0, 0, 0, 0]], [[0, 0, 0]]],
  [
    [[3, 0, -2, -1, 1, 2]],
    [
      [-2, -1, 3],
      [-2, 0, 2],
      [-1, 0, 1],
    ],
  ],
  [[[1, -1, -1, 0]], [[-1, 0, 1]]],
];

getResults<TArgs, TRes>(vals, threeSum);
