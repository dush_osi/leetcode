// used in problems 2, 19, 21
export class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }

  static prepareList(listA: any[]): ListNode | null {
    let list: ListNode | null = null;
    for (let i = listA.length - 1; i >= 0; i--) {
      list = new ListNode(listA[i], list);
    }
    return list;
  }

  static prepareAnswer(list: ListNode | null): any[] {
    const res = [];
    while (list) {
      res.push(list.val);
      list = list.next;
    }
    return res;
  }
}
