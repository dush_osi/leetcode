// https://leetcode.com/problems/add-two-numbers/

import { ListNode } from "./classes";

function addTwoNumbers(l1: ListNode | null, l2: ListNode | null): ListNode | null {
  let result: ListNode | null = null;
  let resArr: ListNode[] = [];
  let ll1 = l1;
  let ll2 = l2;
  let tmpAddNum = 0;
  while (ll1 || ll2 || tmpAddNum) {
    let tmpVal = (ll1?.val || 0) + (ll2?.val || 0) + tmpAddNum;
    if (tmpVal > 9) {
      tmpAddNum = 1;
      tmpVal = tmpVal - 10;
    } else {
      tmpAddNum = 0;
    }
    resArr.push(new ListNode(tmpVal));
    ll1 = ll1 ? ll1.next : null;
    ll2 = ll2 ? ll2.next : null;
  }
  for (let i = resArr.length - 1; i >= 0; i--) {
    result = new ListNode(resArr[i].val, result);
  }
  return result;
}

let list1: ListNode | null = ListNode.prepareList([2, 4, 3]);
let list2: ListNode | null = ListNode.prepareList([5, 6, 4]);

const result = addTwoNumbers(list1, list2);
console.log(ListNode.prepareAnswer(result));
