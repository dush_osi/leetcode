// https://leetcode.com/problems/zigzag-conversion/
import { getResults } from "./assertion";

function convert(s: string, numRows: number): string {
  if (numRows === 1) return s;
  const result: string[] = Array(numRows).fill("");
  let j = 0;
  let k = 0;
  for (let i = 0; i < s.length; i++) {
    result[j] += s[i];
    if (j >= numRows - 1 || k % (numRows - 1) !== 0) {
      j--;
      k++;
    } else {
      j++;
    }
  }
  return result.join("");
}

type TArgs = [string, number];
type TRes = string;
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [["ABC", 1], "ABC"],
  [["PAYPALISHIRING", 3], "PAHNAPLSIIGYIR"],
  [["PAYPALISHIRING", 4], "PINALSIGYAHRPI"],
];

getResults<TArgs, TRes>(vals, convert);
