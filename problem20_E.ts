// https://leetcode.com/problems/valid-parentheses/submissions/897075798/

import { getResults } from "./assertion";

const chars: Record<string, string> = { "(": ")", "{": "}", "[": "]" };

function isValid(s: string): boolean {
  if (s.length == 1) return false;
  let openedStr: string[] = [];
  let i = 0;
  while (i < s.length) {
    if (s[i] in chars) {
      openedStr.push(chars[`${s[i]}`]);
    } else {
      const lastVal = openedStr.pop();
      if (lastVal == undefined || s[i] != lastVal) return false;
    }
    i++;
  }
  return openedStr.length == 0;
}

type TArgs = [string];
type TRes = boolean;

const vals: [TArgs, TRes][] = [
  [["()"], true],
  [["()[]{}"], true],
  [["(]"], false],
  [["{[]}"], true],
  [["("], false],
];
getResults<TArgs, TRes>(vals, isValid);
