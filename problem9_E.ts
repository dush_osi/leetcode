// https://leetcode.com/problems/palindrome-number/
import { getResults } from "./assertion";

function isPalindrome(x: number): boolean {
  if (x < 0) return false;
  let xCopy = x;
  let rev = 0;
  while (xCopy > 0) {
    rev = rev * 10 + (xCopy % 10);
    xCopy = Math.floor(xCopy / 10);
  }
  return rev === x;
}

type TArgs = [number];
type TRes = boolean;
type TIn = [TArgs, TRes];

const testVals: TIn[] = [
  [[121], true],
  [[-121], false],
  [[10], false],
];

getResults<TArgs, TRes>(testVals, isPalindrome);
