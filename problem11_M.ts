// https://leetcode.com/problems/container-with-most-water/
import { getResults } from "./assertion";

function maxArea(height: number[]): number {
  let result = 0;
  let tmp = 0;
  let i = 0;
  let j = height.length - 1;

  while (i < j) {
    if (height[i] > height[j]) {
      tmp = height[j] * (j - i);
      j--;
    } else {
      tmp = height[i] * (j - i);
      i++;
    }
    if (result < tmp) result = tmp;
  }
  return result;
}
type TArgs = [number[]];
type TRes = number;
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [[[1, 8, 6, 2, 5, 4, 8, 3, 7]], 49],
  [[[1, 8, 6, 2, 5, 4, 8, 3, 7]], 49],
  [[[1, 1]], 1],
];
getResults<TArgs, TRes>(vals, maxArea);
