// https://leetcode.com/problems/3sum-closest/
import { getResults } from "./assertion";

function threeSumClosest(nums: number[], target: number): number {
  let result = 0;
  const sorted = nums.sort((a, b) => a - b);

  for (let i = 0; i < sorted.length - 2; i++) {
    if (i > 0 && sorted[i] == sorted[i - 1]) continue;
    let j = i + 1;
    let k = sorted.length - 1;
    if (i === 0) result = sorted[i] + sorted[j] + sorted[k];
    while (j < k) {
      const sum = sorted[i] + sorted[j] + sorted[k];
      if (sum === target) return target;
      if (Math.abs(target - sum) < Math.abs(target - result)) {
        result = sum;
      }
      if (sum < target) {
        j++;
      } else {
        k--;
      }
    }
  }
  return result;
}

type TArgs = [number[], number];
type TRes = number;
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [[[-1, 2, 1, -4], 1], 2],
  [[[0, 0, 0], 1], 0],
  [[[1, 1, 1, 0], 100], 3],
  [[[1, 1, 1, 0], -100], 2],
  [[[4, 0, 5, -5, 3, 3, 0, -4, -5], -2], -2],
  [[[2, 3, 8, 9, 10], 16], 15],
  [[[-1000, -5, -5, -5, -5, -5, -5, -1, -1, -1], -14], -15],
];
getResults<TArgs, TRes>(vals, threeSumClosest);
