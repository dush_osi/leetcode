// https://leetcode.com/problems/roman-to-integer/
import { getResults } from "./assertion";

function romanToInt(s: string): number {
  const nums: Record<string, number> = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
    IV: 4,
    IX: 9,
    XL: 40,
    XC: 90,
    CD: 400,
    CM: 900,
  };
  let curr = 0;
  let res = 0;
  do {
    let twiceSymb = s[curr] + s[curr + 1];
    if (nums[twiceSymb]) {
      res += nums[twiceSymb];
      curr++;
    } else {
      res += nums[s[curr]];
    }
    curr++;
  } while (curr < s.length);

  return res;
}

type TArgs = [string];
type TRes = number;
type TIn = [TArgs, TRes];

const vals: TIn[] = [
  [["III"], 3],
  [["LVIII"], 58],
  [["MCMXCIV"], 1994],
];

getResults<TArgs, TRes>(vals, romanToInt);
