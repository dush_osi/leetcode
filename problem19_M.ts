//https://leetcode.com/problems/remove-nth-node-from-end-of-list/submissions/897042543/
//Solution not my

import { ListNode } from "./classes";

function removeNthFromEnd(head: ListNode | null, n: number): ListNode | null {
  let value = new ListNode();
  value.next = head;
  recursDown(value, n);
  return value.next;
}

function recursDown(head: ListNode | null, n: number): number {
  if (head == null) {
    return 0;
  }
  var value = recursDown(head.next, n);
  if (value == n) {
    head.next = head.next ? head.next.next : head;
  }

  return value + 1;
}

let list: ListNode | null = ListNode.prepareList([1, 2, 3, 4, 5]);

const result = removeNthFromEnd(list, 3);
console.log(ListNode.prepareAnswer(result));
